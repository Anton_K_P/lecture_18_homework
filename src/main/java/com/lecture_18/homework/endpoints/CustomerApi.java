package com.lecture_18.homework.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lecture_18.homework.Customer;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

@RestController
public class CustomerApi {


    private ObjectMapper mapper = new ObjectMapper();
    String commonPath = "/Users/user/IdeaProjects/homework_lect_18/src/main/resources/";
    JSONObject successesResponse = new JSONObject();
    JSONObject failResponse = new JSONObject();


    @PostMapping(path = "/customer", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long id(@RequestBody Customer customer) throws IOException {
        Long customerID = Customer.creatCustomerID();
        customer.save(commonPath + customerID, mapper);
        return customerID;
    }

    @GetMapping(path = "/customer", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    String customer(@RequestParam String id) throws IOException {
        Path path = Path.of(commonPath + id);
        return Files.readString(path);
    }

    @DeleteMapping(path = "/customer/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteCustomer(@PathVariable String id) throws IOException, JSONException {
        Path path = Path.of(commonPath + id);
        if (Files.deleteIfExists(path)) {
            successesResponse.put("success", true);
            return successesResponse.toString();
        }failResponse.put("Fail", false);
        return failResponse.toString();
    }
}
