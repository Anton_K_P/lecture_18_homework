package com.lecture_18.homework;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class Customer {
    private String age;
    private String name;
    private String sum;

    public void setAge(String age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getSum() {
        return sum;
    }

    public void save(String path, ObjectMapper mapper) throws IOException {
        mapper.writeValue(new File(path), this);

    }

    public static Long creatCustomerID() {
        Long id = new Date().getTime();
        return id;
    }
}
